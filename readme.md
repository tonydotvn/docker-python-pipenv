# Python Web Service Base Docker Image

This consists of python (with pipenv, gunicorn installed) and nginx as a web reverse proxy.
You will save a lot of life when don't need to care about how to deploy your microservice via docker.

## Contributors

- Kingsley Huynh <kingsleyvn@gmail.com>
