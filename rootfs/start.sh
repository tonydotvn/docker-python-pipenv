#!/bin/bash

# Set custom webroot
if [ ! -z "$WEBROOT" ]; then
  webroot=$WEBROOT
  sed -i "s#root /app/public;#root ${webroot};#g" /etc/nginx/sites-available/default.conf
else
  webroot=/app/public
fi

if [ ! -z "$SERVICE_80_NAME" ]; then
  service=$SERVICE_80_NAME
elif [ ! -z "$SERVICE_NAME" ]; then
  service=$SERVICE_NAME
else
  service="nginx"
fi

if [ ! -z "$SERVICE_TAGS" ]; then
  service_env=$SERVICE_TAGS
elif [ ! -z "$SERVICE_ENV" ]; then
  service_env=$SERVICE_ENV
else
  service_env="dev"
fi

# Allow run custom script
if [ ! -z "$SCRIPT" ] && [ -f "$SCRIPT" ]; then
  chmod a+x $SCRIPT
  . $SCRIPT
fi

if [ -f /app/resources/docker/hook-start ]; then
    source /app/resources/docker/hook-start
fi

# inject additional nginx location
if [ -n "${NGINX_CUSTOM_LOCATION}" ]; then
  sed -i "s!#-additional-location-#!include ${NGINX_CUSTOM_LOCATION};!g" /etc/nginx/sites-available/default.conf
fi

if [ -n "$GUNICORN_APP_NAME" ]; then
    cd /app
    /usr/local/bin/gunicorn -b 0.0.0.0:8000 \
        --worker-class gevent \
        --workers 2 \
        --max-requests 1000 \
        --error-logfile=- \
        "$GUNICORN_APP_NAME" --daemon
fi

nginx -g "daemon off;"
