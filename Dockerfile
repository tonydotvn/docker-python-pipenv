FROM python:3.7-alpine

RUN pip install pipenv gunicorn

RUN apk update \
    && apk add \
        gcc python-dev bash \
        musl-dev \
        bsd-compat-headers \
        libevent-dev \
        nginx \
    # && addgroup -g 82 -S www-data \
    && adduser -u 82 -D -S -G www-data www-data \
    && mkdir -p /tmp/nginx \
    && chown -R www-data /tmp/nginx \
    && rm -rf /var/cache/apk/* \
    && rm -Rf /etc/nginx/nginx.conf \
    && mkdir -p /etc/nginx/sites-available/ \
    && mkdir -p /etc/nginx/sites-enabled/

ADD rootfs /

RUN mkdir -p /run/nginx/ \
    && ln -sf /etc/nginx/sites-available/default.conf /etc/nginx/sites-enabled/default.conf \
    && nginx -t
EXPOSE 80
RUN chmod a+x /start.sh
STOPSIGNAL SIGTERM
CMD ["/start.sh"]
